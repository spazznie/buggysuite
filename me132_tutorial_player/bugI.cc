#include <libplayerc++/playerc++.h>
#include <libplayerc++/playerc++.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include "cmdline_parsing.h"
#include "common_functions.h"
#include "occupancy_grid.h"

using namespace PlayerCc;
using namespace std;
 
double PI = atan(1)*4;

// Thresholds for checking if two values are equal
double THRESHOLD = 0.05;       // Distance threshold
double qH_L_THESHOLD = 0.5;    // Distance threshold to check if robot has reached
                               // a qH(obstacle hitting pt) or qL (obstacle leaving pt).
                               // This threshold is relatively large becaue robots does
                               // not maintain a set distance to the wall perfectly.
double THETA_THRESHOLD = 0.05; // Theta threshold
double THETA_EPSILON = 0.01;

// The length of the robot
double ROBOT_LENGTH = 0.4; 
// Extra insurance to prevent smashing into walls
double SLACK = 0.3;
// 
double OVERSHOOT = 0.2;
// Robot's vision during boundary following
// Assume the robot can see at most ROBOT_BF_VISION during BF. We don't want
// the robot to consider other objects (chair, desk, and etc) as
// the current obstacle it is trying to circumnavigate.
double ROBOT_BF_VISION = (ROBOT_LENGTH + SLACK)*2;

// Robot's speed.
double FORWARD_RATE = 0.2;
// Robot's turnrate. This is the tiny turnrate.
// Robots can use this to turn closer/farther away from the wall
double ROBOT_MAINTAIN_DISTANCE_TURNRATE = 0.2;
// Robot use this turnrate to reseek boundaries when the obstacle boundary
// was found then lost. This is useful when robot is turning cornors. It 
// stay and turn slowly to find the boundary.
double ROBOT_RESEEK_TURNRATE = 0.3;

// Finding the angle between two vectors, a vector from (x2, y2) to (x1, y1),
// and a vector from (x2, y2) to (x3, y3).
double findAngleBetweenVectors(double x1, double y1,
                               double x2, double y2,
                               double x3, double y3) {
  // Find the first vector
  double v1x = x1 - x2;
  double v1y = y1 - y2;
	
  // Find the second vector
  double v2x = x3 - x2;
  double v2y = y3 - y2;
  
  // Find the angle between two vectors using A dot B = ||A|| * ||B|| * cos(theta)
  double denom = sqrtf(v1x*v1x + v1y*v1y) * sqrtf(v2x*v2x + v2y*v2y);
  double angle = ((v1x * v2x) + (v1y * v2y)) / denom;
  angle = acos(angle);
  
  // if v1 is below v2, the vector is in the negative quadrants and hence the
  // angles are negative
  if (v1y < -0.05)
    angle = -angle;
  
  return angle;
}

// In BF, this method returns the direction the robot should face
double getDirection(vector<double> range_data,
                    vector<double> bearing_data, 
                    double posX, 
                    double posY,
                    double theta,
                    double &currMinDistance,
                    bool &found,
                    bool &lost) {
  // Assume the robot can see at most minDistance. We don't want
  // the robot to consider other objects (chair, desk, and etc) as
  // the current obstacle it is trying to circumnavigate. 
  double minDistance = ROBOT_BF_VISION; 
  
  int minIndex = -1;
  int maxIndex = -1;
  
  // We want to find the closest and farthest points on the wall that robot can
  // see. The direction of the vector formed by these two points should be
  // the desired direction of the robot.
  // Only use portion of data to avoid considering other objects as wall.
  for (int i = 270; i < 360; i++) {
    
    if (range_data[i] <= minDistance) {
      // Assume the range data are continous (which is true in our boundary following
      // case), the closest and farthest points should be the first and last range
      // data points.
      if (minIndex == -1)
        minIndex = i;
      else
        maxIndex = i;
    }
    
    // Keep tracks of the distance between the robot and the wall
    currMinDistance = std::min(range_data[i], currMinDistance);
  }
    
  // If it did not find any object, set its status to lost, and return
  // it's current pose as the desired angle. The lost status along with
  // found indicates when a wall was being followed but is now no longer
  // in sight. It forces the robot to move counter-clockwise inplace 
  // until it refinds the obstacle.
  if (minIndex == -1 || maxIndex == -1 || minIndex == maxIndex)
  {
    lost = true;
    return theta;
  }
  
  // It has found a wall
  found = true;
  lost = false;
  
  // Convert first point of intersection to global coordinates
  double x1 = posX + range_data[minIndex]*cos(theta + bearing_data[minIndex]);
  double y1 = posY + range_data[minIndex]*sin(theta + bearing_data[minIndex]);
  
  // Convert end point of intersection to global coordinates
  double x2 = posX + range_data[maxIndex]*cos(theta + bearing_data[maxIndex]);
  double y2 = posY + range_data[maxIndex]*sin(theta + bearing_data[maxIndex]);
  
  // Create a vector represnting the x-axis
  double x3 = x2 + 100;
  double y3 = y2;
  
  // Find angle between the wall and the x-axis
  double angle = findAngleBetweenVectors(x1, y1, x2, y2, x3, y3);
  
  return angle;
}

// The boundary following method. It sets the correct speed and turnrate, and it
// adds the already-seen obstacle points to a set to keep track.
void boundaryFollowing(vector<double> range_data,
                       vector<double> bearing_data, 
                       double posX,
                       double posY,
                       double theta,
                       bool &found,
                       bool &lost,
                       double &speed,
                       double &turnrate,
                       set<pair<int, int> > &obstacle_points) {
  turnrate = 0;
  speed = FORWARD_RATE;
  
  // Store the min distance from the robot to the wall. We can use this number
  // to decide whether we want to move closer/away from the wall.
  double currMinDistance = 99999; // an arbitrary large number
  
  // Convert a point on the obstacle to global coordinates
  double obs_x = posX + range_data[360]*cos(theta + bearing_data[360]);
  double obs_y = posY + range_data[360]*sin(theta + bearing_data[360]);
  // Store this obstacle point
  obstacle_points.insert(make_pair((int)obs_x, (int)obs_y));
  
  // Getting the desired direction of the robot
  double angle =  getDirection(range_data,
                               bearing_data, 
                               posX, 
                               posY,
                               theta,
                               currMinDistance,
                               found,
                               lost);
  
  // Make both angle and robot orientation between 0 to 2PI
  if (angle < 0)
    angle += (2 * M_PI);
  
  if (theta < 0)
    theta += (2 * M_PI);
  
  // The robot laser sensing bearing data is very badly designed.
  // The following lines scale the angles so that differences reflect
  // the actual differences between the desired and true orientations.
  // For eg. Desired = 5.2, Actual = -0.3. This would cause the turn-rate
  // to be 5.5 instead of the actual -0.8
  if (abs(angle - theta) > M_PI)
  {
    if (theta > M_PI)
      angle = angle + (2 * M_PI);
    else if (angle > M_PI)
      theta = theta + (2 * M_PI);
    
  }
  
  // Find the amount the robot needs to turn
  turnrate = angle - theta;
  
  printf("Angle: %f, Orientation: %f, Turnrate: %f, Min Distance: %f\n", angle, theta, turnrate, currMinDistance);
  
  // If it gets too close to the object, try to move away.
  if (currMinDistance < ROBOT_LENGTH + SLACK) {
    turnrate -= ROBOT_MAINTAIN_DISTANCE_TURNRATE;
  }

  // If it gets too far, it will try to move closer
  if (currMinDistance >= ROBOT_LENGTH + SLACK + 0.1) {
    turnrate += ROBOT_MAINTAIN_DISTANCE_TURNRATE;
  }

  // Condition that forces wall following. Once a boundary is found
  // it will try to reseek it as soon as it is no longer visible in the
  // laser data
  if (found && lost)
  {
    speed = 0;
    turnrate = ROBOT_RESEEK_TURNRATE;
  }
  
}

// We first find the segment between the robot and the obstacle, then find the angle
// between the segment and x-axis. This method return robot's angle if it
// is facing the wall. Using this angle, we rotate the robot such that
// the wall is on the left side of the robot.
double getDirectionInBFBegin(vector<double> range_data,
                             vector<double> bearing_data, 
                             double posX, 
                             double posY,
                             double theta) {
  // Assume the robot can see at most minDistance. We don't want
  // the robot to consider other objects as the wall (chair, desk, and etc)
  double minDistance = ROBOT_BF_VISION; 
  
  // Store the current min distance to the wall
  double currMinDistance = 99999; // an arbitrary large distance
  int minIndex = -1;
  
  
  // We want to find the closest obstacle point to the robot
  for (int i = 0; i < 360; i++)
  {
    if (range_data[i] <= minDistance && range_data[i] < currMinDistance)
    {
      currMinDistance = range_data[i];
      minIndex = i;
    }
  }
  
  // Convert obstacle point to global coordinates
  double x1 = posX + range_data[minIndex]*cos(theta + bearing_data[minIndex]);
  double y1 = posY + range_data[minIndex]*sin(theta + bearing_data[minIndex]);
  
  // Create a vector represnting the x-axis
  double x3 = posX + 100;
  double y3 = posY;
  
  // Now we have a vector connecting the robot and the obstacle. 
  // Find angle between the segment and the x-axis
  double angle = findAngleBetweenVectors(x1, y1, posX, posY, x3, y3);
  
  return angle;
}

// During the BFBegin stage, we first want to turn the robot such that the obstacle
// wall is on the left side of the robot. This is to make sure that robot always
// circumnavigates counterclockwise. The robot will turn slowly, until the
// difference between desired and actual orientation < PI/5.
// This funciton return true when the desired orientation is reached.
bool boundaryFollowingBFBegin(vector<double> range_data,
                       vector<double> bearing_data, 
                       double posX,
                       double posY,
                       double theta,
                       double &speed,
                       double &turnrate) {
  turnrate = 0;
  speed = 0;
  
  // Find the angle where robot is facing the wall.
  double angle =  getDirectionInBFBegin(range_data,
                               bearing_data, 
                               posX, 
                               posY,
                               theta);
  
  // Minus 90 degree to make sure the wall is always on the left.
  angle = angle - M_PI/2;

  // Make both angle and robot orientation between 0 to 2PI
  if (angle < 0)
    angle += (2 * M_PI);
  
  if (theta < 0)
    theta += (2 * M_PI);
  
  // The robot laser sensing bearing data is very badly designed.
  // The following lines scale the angles so that differences reflect
  // the actual differences between the desired and true orientations.
  // For eg. Desired = 5.2, Actual = -0.3. This would cause the turn-rate
  // to be 5.5 instead of the actual -0.8
  if (abs(angle - theta) > M_PI) {
    if (theta > M_PI)
      angle = angle + (2 * M_PI);
    else if (angle > M_PI)
      theta = theta + (2 * M_PI);
  }
  
  // Turn slowly clockwise.
  turnrate -= 0.2;
  
  // Find the amount the robot needs to turn. Return true if it is in the desired direction.
  double actualTurnrate = angle - theta; 
  
  printf("Angle: %f, Orientation: %f, Turnrate: %f\n", angle, theta, actualTurnrate);
  if (abs(actualTurnrate) < PI/5)
    return true;
  else
    return false;
}

// tells the robot to go to the point
// return r_dot and theta_dot that can be used as inputs to setSpeed
int go_to_point(double goal_x, double goal_y, double robot_x, double robot_y,
    double robot_theta, double& r_dot, double& theta_dot){
    //find the distance from the current point to the goal point
  
    r_dot = 0;
    int theta_dot_sign = 1;
    //depending on the sign of the thetas, decide which direction to rotate
    double dx = goal_x - robot_x;
    double dy = goal_y - robot_y;
    double Dx = dx / sqrt((dx*dx) + (dy*dy));
    double Dy = dy / sqrt((dx*dx) + (dy*dy));
    double Hx = cos(robot_theta);
    double Hy = sin(robot_theta);
    double unit_dist  = (sqrt(pow((Dx - Hx),2) + pow((Dy - Hy),2)));
    double cross_z = Hx*Dy - Hy*Dx;
    theta_dot_sign = ((cross_z < 0)? -1 :1);
    theta_dot = 0.3;
    if (unit_dist < THETA_THRESHOLD){
      //the case where the theta is correct
      theta_dot = 0;
      //set r_dot to the maximum
      double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
      r_dot = std::min(distance_to_goal + 0.1, FORWARD_RATE);
    } else if (unit_dist < THETA_EPSILON) {
      //in a more acceptable range, so allow some movement
      theta_dot = 0;
      r_dot = FORWARD_RATE;
    } else if (unit_dist < 0.3) {
      //snap to minimum rotation requirement
      theta_dot = theta_dot_sign * 0.3;
      r_dot = 0;
    } else {
      //restrict to maximum rotation requirement
      theta_dot = theta_dot_sign * std::min(unit_dist, 0.7);
      r_dot = 0;
    }
    return 0;
}

// Given current destination and desired destination, determine if we are
// within t of the goal. 
int reached_within(double goal_x, double goal_y, double robot_x, double robot_y, double t) {
   
    double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
    return  ((distance_to_goal <= t) ? 1: 0);
}

// Given current destination and desired destination, determine if we are
// within THRESHOLD of the goal. 
int reached(double goal_x, double goal_y, double robot_x, double robot_y) {
    // apply the distance between two points formula and determine if it is
    // <= THRESHOLD
    return reached_within(goal_x, goal_y, robot_x, robot_y, THRESHOLD);
}

// Calculate distance between two points.
double distance(double goal_x, double goal_y, double robot_x, double robot_y) {
  
    double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
    return distance_to_goal;
}

// Check if the robot has encountered an obstacle
// A robot has encountered a obstacle if:
// 1. robot is very close to an obstacle
// 2. robot has never seen this obstacle before.
//
// Note: if a robot is very close to an obstacle and has seen it before,
// it is probably trying to leave the obstacle to reach another point.
// This is to avoid robot turning back to BF state on the same obstacle after it
// has finished circumnagivate and reached the leaving point.
bool obstacleEncountered(vector<double> range_data, vector<double> bearing_data,
                         double posX, double posY, double theta, set<pair<int, int> > obstacle_points) {
  bool retval = false;
  for (int i = 0; i < range_data.size(); ++i) { 
    // Need to + 0.2 because the robot always overshoot. SLACK is the ideal distance between
    // robot and the wall.
    if (range_data[i] <= (ROBOT_LENGTH + SLACK + 0.2)) {
     
       // Convert first point of intersection to wall co-ordinates
       double x1 = posX + range_data[i]*cos(theta + bearing_data[i]);
       double y1 = posY + range_data[i]*sin(theta + bearing_data[i]);
       if (obstacle_points.size() == 0 || obstacle_points.find(make_pair((int) x1, (int) y1)) == obstacle_points.end())
         retval = true;
    };
  }
  return retval; 
}

// Check if it's in immediately crashing danger
bool crashing(vector<double> range_data, vector<double> bearing_data){
              
  for (int i = 180-10; i < 180+10; i++) {
    if (range_data[i] < ROBOT_LENGTH) {
       return true;
    }
  }
  return false;
}
    

enum state {
  MTG,         // Motion to goal
  BF_BEGIN,    // Turn the robot such that the wall is on
               // it's left at the beginning of BF
  BF,          // Start boundary following
  BF_FOUND_QL, // The state after circumnavigating. Robot has found qL
               // and trying to reach it.
}; 

int main(int argc, char **argv)
{

  // Wrote our own arg parser .-.
  if (argc != 7) {
    cout << "Usage: ./bugI -p <port> -x <goal_x> -y <goal_y> " << endl;
    exit(1);
  }
  gPort = atoi(argv[2]);
  
  double qf_x = atof(argv[4]); //q_f 
  double qf_y = atof(argv[6]); //q_f
  cout << "heading to goal " << qf_x << "," << qf_y  << endl;
  
  // Store path data
  string pathfilename = "../data/pathdata/bugI"; 
  ofstream pathfile;
  pathfile.open(pathfilename.c_str());
  cout << "writing path data to" << pathfilename << endl;
  int stepcount = 0;
  
  double lower_left[2] = {-8, -8};
  double upper_right[2] = {8, 8};
  double cell_size = 0.05;

  SimpleOccupancyGrid oc(lower_left, upper_right, cell_size);

  try {
    // Initialize connection to player
    PlayerClient robot(gHostname, gPort);
    Position2dProxy pp(&robot, gIndex);
    LaserProxy lp(&robot, gIndex); 

    int num_attempts = 20;
    if(!check_robot_connection(robot, pp, num_attempts)) 
      exit(-2);

    // The state our FSM is in
    int state = 0; 
    // Robot pose values
    double robot_x, robot_y, robot_theta; 
    // Robot speed setting values
    double r_dot, theta_dot; 

    // Count the number of step robot took
    int step = 0;
    
    // Values that can be updated in the loop
    // by default, assume goal is reachable. update this as seen fit.
    bool unreachable = false;
    // qH  = the point at which the robot hits an obstacle and starts to circumnavigate
    double qH_x = 0;
    double qH_y = 0;
    // qL = the leaving point of the current obstacle
    double qL_x = 0;
    double qL_y = 0;
    // A set of points on the obstalce it have seen
    set<pair<int, int> > obstacle_points;  
    
    // Variables for boundary following
    // In order to find qL we need to keep track of min distance from boundry point
    // to the goal
    double minDistanceBF = 99999;
    // True if the robot has ever found a wall.
    bool found = false; 
    // Indicate if the robot can detect any object. It is set to false
    // if the robot cannot see any object in the current pose.
    bool lost = false;
    // Keep track of the step when robot enter BF state, in order to make sure we
    // don't exit BF early
    int prevStep = 0;
    
    while(1) {
			
      // Read from the proxies; YOU MUST ALWAYS HAVE THIS LINE
      robot.Read();
			
      // Collect robot pose data
      double pose[3] = { pp.GetXPos(), pp.GetYPos(), pp.GetYaw() };

      robot_x = pose[0];
      robot_y = pose[1];
      robot_theta = pose[2];

      // Write data to file every 5 steps
      stepcount = stepcount % 5;
      if (stepcount == 0) {
        pathfile << robot_x << " " << robot_y << endl;
      }
      stepcount++;

      // Collect laser data
      unsigned int n = lp.GetCount();
						
      if (n==0)
        //no data available
        continue;
			
      vector<double> range_data(n);
      vector<double> bearing_data(n);
      for(uint i=0; i<n; i++) {
        range_data[i] = lp.GetRange(i);
        bearing_data[i] = lp.GetBearing(i);
      }

      // Check if obstacle is encounterd.
      bool encounteredObstacle = obstacleEncountered(range_data, bearing_data, robot_x, robot_y, robot_theta,
                                                     obstacle_points); 

      // Check for completeness
      if (reached(qf_x, qf_y, robot_x, robot_y)) {
        cout << "goal reached " << endl;
        exit(0);
      } else if (unreachable) { 
        cout << "goal can't be reached. Failure" << endl;
        exit(1);
      } else {
        step++;
        switch (state)  {
        
          case MTG: 
            printf("in MTG\n");
            
            // If we are near an obstacle, switch to BF mode
            if (encounteredObstacle) {
              
              // Record the coordinates of qH as the current point where the wall was encountered
              qH_x = robot_x;
              qH_y = robot_y;

              state = BF_BEGIN;
            } else {
              
              // If the obstacle is in immediate crashing danger but has not switch to
              // BF yet, it means the goal is unreachable (in side the obstacle).
              if (crashing(range_data, bearing_data)) {
                unreachable = true;
              } else {
                // Just keep moving toward the goal until we encounter a wall
                go_to_point(qf_x, qf_y, robot_x, robot_y, robot_theta, r_dot, theta_dot);
                pp.SetSpeed(r_dot, theta_dot);
              } 
            }
            break;
          
          case BF_BEGIN:
            printf("in BF_BEGIN\n");
            
            // Turn the robot such that the wall is on its left. It will exit this state,
            // when the robot is in desired orientation.
            if(boundaryFollowingBFBegin(range_data, bearing_data, robot_x, robot_y, robot_theta,
                              r_dot, theta_dot))
              state = BF;
            else 
              pp.SetSpeed(r_dot, theta_dot);
            
            break;
          
          case BF: 
            printf("in BF\n");
            
            // Keep track when the robot is enters BF.
            if (prevStep == 0) {
              prevStep = step;
            }
            
            if (reached_within(qH_x, qH_y, robot_x, robot_y, qH_L_THESHOLD) && step - prevStep > 50) {
              printf("complete following the obstacle\n");

              // Move to next state and reset variables.
              state = BF_FOUND_QL;
              prevStep = 0;
              minDistanceBF = 99999;
              found = false; 
              lost = false;
            } else {
              
              // Keep track of the samllest distance to the wall, so robot can return
              // here after circumnavigation.
              double currDistanceBF = distance(qf_x, qf_y, robot_x, robot_y);
              if (currDistanceBF < minDistanceBF) {
                minDistanceBF = currDistanceBF;
                qL_x = robot_x;
                qL_y = robot_y;
              }

              // Continue boundary following.
              boundaryFollowing(range_data, bearing_data, robot_x, robot_y, robot_theta,
                                found, lost, r_dot, theta_dot, obstacle_points);
              pp.SetSpeed(r_dot, theta_dot);
           } 
           break;
          
          case BF_FOUND_QL:
            printf("in BF_FOUND_QL\n");
            
            // Continue moving until it reaches qL
            if (!reached_within(qL_x, qL_y, robot_x, robot_y, qH_L_THESHOLD)) {
              boundaryFollowing(range_data, bearing_data, robot_x, robot_y, robot_theta,
                                found, lost, r_dot, theta_dot, obstacle_points);
              pp.SetSpeed(r_dot, theta_dot);
            } else {
              
              // If it is at qL, start moving toward the goal
              qL_x = 0;
              qL_y = 0;
              state = MTG;
              
              go_to_point(qf_x, qf_y, robot_x, robot_y, robot_theta, r_dot, theta_dot);
              pp.SetSpeed(r_dot, theta_dot);
            }
            break;
            
        }
     }
   }
  } catch(PlayerError e) {
    write_error_details_and_exit(argv[0], e);
  }

  return 0;
}

