#include <libplayerc++/playerc++.h>
#include <libplayerc++/playerc++.h>
#include <stdio.h>
#include <math.h>
#include <vector>

#include "cmdline_parsing.h"
#include "common_functions.h"
#include "occupancy_grid.h"

using namespace PlayerCc;
using namespace std;

/* ASSUMPTIONS
 * Initially, the robot must be placed perpendicular to one side of the
 * L-obstacle such that it can detect that there is a wall in front. It can
 * start any distance away from this wall in front, but the distance should
 * be larger than the WALL_DISTANCE.
 * 
 * As the robot moves around the obstacle, we assume that there are no other
 * obstacles surrounding the L-obstacle within WALL_DISTANCE meters.
 * WALL_DISTANCE represents the distance the robot maintains away from the
 * L-obstacle
 *
 * We assume that the environment is static and that the L does not move
 * from its starting orientation while the robot is traversing.
 *
 * Finally, we assume that the robot makes near-perfect turns and movements
 */
 
/* gotopoint */
double PI = atan(1)*4;

/* lab values
 * THRESHOLD = 0.05;
 * THETA_THRESHOLD = PI/18
 * THETA_EPSILON = PI/18;
 */
double THRESHOLD = 0.05; // threshold for goal
double THETA_THRESHOLD = 0.05;
double THETA_EPSILON = 0.01;

/* /gotopoint */
//lab value TARGET_ROBOT_THETA_THRESHOLD = PI/18 
double TARGET_ROBOT_THETA_THRESHOLD = 0.1; 

//conversion factor for scaling
double CONVERSION = 1; 
//the length of the robot
double ROBOT_LENGTH = 0.2; 
//distance we wish to maintain
double MAINTAIN_DISTANCE = 0.6; 
//how close we allow walls
double WALL_DISTANCE = MAINTAIN_DISTANCE + ROBOT_LENGTH; 
//breathing room after wall runs out
double OVERSHOOT_DISTANCE = MAINTAIN_DISTANCE; 

// how fast to move forward
//lab value 0.075
double FORWARD_RATE = 0.75; 
// how fast to turn robot
double TURN_RATE = 0.3; 

/* tells the robot to go to the point
 * return r_dot and theta_dot that can be used as inputs to setSpeed
 */
int go_to_point(double goal_x, double goal_y, double robot_x, double robot_y,
    double robot_theta, double& r_dot, double& theta_dot){
    //find the distance from the current point to the goal point
  
    r_dot = 0;
    int theta_dot_sign = 1;
    //depending on the sign of the thetas, decide which direction to rotate
    double dx = goal_x - robot_x;
    double dy = goal_y - robot_y;
    double Dx = dx / sqrt((dx*dx) + (dy*dy));
    double Dy = dy / sqrt((dx*dx) + (dy*dy));
    double Hx = cos(robot_theta);
    double Hy = sin(robot_theta);
    double unit_dist  = (sqrt(pow((Dx - Hx),2) + pow((Dy - Hy),2)));
    double cross_z = Hx*Dy - Hy*Dx;
    theta_dot_sign = ((cross_z < 0)? -1 :1);
    theta_dot = 0.3;
    if (unit_dist < THETA_THRESHOLD){
      //the case where the theta is correct
      theta_dot = 0;
      //set r_dot to the maximum
      double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
      r_dot = std::min(distance_to_goal + 0.1, FORWARD_RATE);
    } else if (unit_dist < THETA_EPSILON) {
      //in a more acceptable range, so allow some movement
      theta_dot = 0;
      r_dot = FORWARD_RATE;
    } else if (unit_dist < 0.3) {
      //snap to minimum rotation requirement
      theta_dot = theta_dot_sign * 0.3;
      r_dot = 0;
    } else {
      //restrict to maximum rotation requirement
      theta_dot = theta_dot_sign * std::min(unit_dist, 0.7);
      r_dot = 0;
    }
    return 0;
}

/* given current destination and desired destination, determine if we are
 * within THRESHOLD of the goal. 
 * @param
 *  goal_x, goal_y = the coordinates of the goal destination
 *  robot_x, robot_y = the coordinates of the robot
 * @return
 *  0/false if we have not reached the goal within the threshold
 *  1/true otherwise
 */
int reached(double goal_x, double goal_y, double robot_x, double robot_y) {
    /* apply the distance between two points formula and determine if it is
     * <= THRESHOLD
     */
    double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
    return  ((distance_to_goal <= THRESHOLD) ? 1: 0);
}

/* detect if there is a wall on the left
 * @param
 *   range_data = laser range data
 *   bearing_data = laser bearing data
 *
 * @return
 *   true if there is a wall, false if there is not
 */ 
bool wallOnLeft(vector<double> range_data, vector<double> bearing_data) {
  bool retval = false;
  for (uint i = range_data.size() - 1; i > range_data.size()/2; --i) {
    if (range_data[i] <= WALL_DISTANCE*CONVERSION) {
      retval =true;
    }
  }
  return retval;
}

// sweep through a cone of angle cone_range
/* return true if there is a wall in front of the robot
 * @param
 *   range_data = laser range data
 *   bearing_data = laser bearing data 
 *
 * @return true if there is a wall in front; false if there is not
 */
bool wallInFront(vector<double> range_data, vector<double> bearing_data) {
  int cone_range = range_data.size() / 8; // the degrees of the cone is 2 *  cone_range
  int cone_left = range_data.size()/2 - cone_range;
  int cone_right = range_data.size()/2 + cone_range;
  bool retval = false;
  for (int i = cone_left; i < cone_right; ++i) { 
    if (range_data[i] < (WALL_DISTANCE * CONVERSION)) {
      retval = true;
    };
  }
  return retval; 
}

int main(int argc, char **argv)
{
  /* Calls the command line parser */
  parse_args(argc, argv);

  double lower_left[2] = {-8, -8};
  double upper_right[2] = {8, 8};
  double cell_size = 0.05;

  SimpleOccupancyGrid oc(lower_left, upper_right, cell_size);

  try {
    /* Initialize connection to player */
    PlayerClient robot(gHostname, gPort);
    Position2dProxy pp(&robot, gIndex);
    LaserProxy lp(&robot, gIndex); 

    int num_attempts = 20;
    if(!check_robot_connection(robot, pp, num_attempts)) 
      exit(-2);

    //the state our FSM is in
    int state = 0; 
    //robot pose values
    double robot_x, robot_y, robot_theta; 
    //robot speed setting values
    double r_dot, theta_dot; 
    //coordinates of next goal point
    double goal_x, goal_y;  
    //the direction the robot is facing *ideally* within its starting reference frame,
    // which has it facing 90 degrees
    int target_robot_theta_deg = 90; 
    //the radian version of target_robot_theta_deg
    double target_robot_theta_rad = target_robot_theta_deg * (PI/180);

    while(1) {
			
      //read from the proxies; YOU MUST ALWAYS HAVE THIS LINE
      robot.Read();
			
      //collect robot pose data
      double pose[3] = { pp.GetXPos(), pp.GetYPos(), pp.GetYaw() };

      robot_x = pose[0];
      robot_y = pose[1];
      robot_theta = pose[2];

      //collect laser data
      unsigned int n = lp.GetCount();
						
      if (n==0)
        //no data available
        continue;
			
       vector<double> range_data(n);
       vector<double> bearing_data(n);
       for(uint i=0; i<n; i++) {
         range_data[i] = lp.GetRange(i);
         bearing_data[i] = lp.GetBearing(i);
       }

      //check for wall on left and right, for use depending on which
      // state we are currently in
      bool wallonleft = wallOnLeft(range_data, bearing_data); 
      bool wallinfront = wallInFront(range_data, bearing_data);
      //UNCOMMENT IN LAB: thetas are calculated differently in simulation
      //if (target_robot_theta_rad < 0) target_robot_theta_rad += 2 * PI; 
      //calculate difference between L-shaped obstacle's axis and reference
      // frame axis
      int diffTheta = 0;

      /* FSM for wall-following
       * States
       * 0: start
       *    we're just moving forward until we encounter a wall in front of us
       * 1: moving forward with a wall on the left side
       *    if the left wall disappears, calculate goal point based on overshoot value
       *      -> state 4
       *    if there is a wall in front -> state 3
       *    otherwise continue moving forward -> state 1
       * 2: turning right
       *    if we have finished turning right -> state 1
       *    otherwise continue turning right -> state 2
       * 3: wall in front
       *    if there is a wall in front of us -> state 2
       * 4: overshooting
       *    if goal point reached -> state 6
       *    otherwise, keep moving to the goal location -> state 4
       * 5: searching for a left wall
       *    if left wall found -> state 1
       *    if wall in front -> state 3 (prevent crashing)
       *    otherwise, continue moving forward -> state 5
       * 6: turning left
       *    if we have finished turning left -> state 5
       *    otherwise, continue turning left -> state 6
       */  
      switch (state)  {
        case 0: //start stage
          // just keep moving forward until we encounter a wall
          pp.SetSpeed(FORWARD_RATE, 0);
          diffTheta = 90 - robot_theta;
          if (wallinfront) {
            state = 3;
            break;
          } 
          break;
        case 1: //moving forward stage
          // if there is a wall in front -> turn right state
          if (wallinfront) {
            pp.SetSpeed(0,0);
            state = 3;
            break;
          } else if (!wallonleft) {
            //if there is no more left wall -> move forward without left wall 
            //determine overshoot position depending on the orientation of the robot
            //  in its shifted reference frame
            switch (target_robot_theta_deg + diffTheta) {
              case 0:
              case 360:
              case -360:
                goal_x = robot_x + (OVERSHOOT_DISTANCE * CONVERSION);
                goal_y = robot_y;
                break;
              case 90:
              case -270:
                goal_x = robot_x;
                goal_y = robot_y + (OVERSHOOT_DISTANCE * CONVERSION);
                break;
              case 180:
              case -180:
                goal_x = robot_x - (OVERSHOOT_DISTANCE * CONVERSION);
                goal_y = robot_y;
                break;
              case 270:
              case -90:
                goal_x = robot_x;
                goal_y = robot_y - (OVERSHOOT_DISTANCE * CONVERSION);
                break;
            }
            state = 4;
            break;
          } else {
            // otherwise keep moving in a straight line; we still have a
            // wall on the left
            pp.SetSpeed(FORWARD_RATE, 0);
            state = 1;
            break;
          }
          break;
        case 2: //turning right (after we have encountered a wall in front)
          if ((abs(target_robot_theta_rad - robot_theta)) > TARGET_ROBOT_THETA_THRESHOLD) {
            pp.SetSpeed(0, -TURN_RATE);
            break;
          } else {
            pp.SetSpeed(0,0);
            state = 1; // move forward again
            break;
          } 
          break;
        case 3: //wallinfront
          target_robot_theta_deg = (target_robot_theta_deg - 90) % 360;
          if (target_robot_theta_deg > 180) {
            target_robot_theta_deg = -(360 - target_robot_theta_deg);
          }
          target_robot_theta_rad = (target_robot_theta_deg) * (PI/180);
          pp.SetSpeed(0,0); //stop moving
          state = 2; 
          break;
       case 4: // moving without wall on left; overshoot so we have room
          if (reached(goal_x, goal_y, robot_x, robot_y)) {
            //if we have reached our overshoot goal, prepare to turn left
            pp.SetSpeed(0,0);
            target_robot_theta_deg = (target_robot_theta_deg + 90) % 360;
            if (target_robot_theta_deg > 180) {
              target_robot_theta_deg = -(360 - target_robot_theta_deg);
            }
            target_robot_theta_rad = (target_robot_theta_deg) * (PI/180);
            state = 6;
          } else {
            //otherwise continue trying to reach the overshoot goal
            go_to_point(goal_x, goal_y, robot_x, robot_y, robot_theta, r_dot, theta_dot);
            pp.SetSpeed(r_dot, theta_dot);
            state = 4;
          }
          break;
       case 5: // searching for left wall
          pp.SetSpeed(FORWARD_RATE, 0);
          if (wallonleft) {
            //found a wall on the left
            //change state to moving forward
            state = 1;
          } else {
            //prevent crashing into walls. 
            if (wallinfront) 
            	state = 3;
	    else {
            //continue moving forward
            state = 5;
            }
          }
          break;
       case 6: //turning left
          if ((abs(target_robot_theta_rad - robot_theta)) > TARGET_ROBOT_THETA_THRESHOLD) {
            pp.SetSpeed(0, TURN_RATE);
            break;
          } else {
            pp.SetSpeed(0,0);
            // finished turning left, search for the wall
            state = 5;
            break;
          } 
 
      }
   }
  } catch(PlayerError e) {
    write_error_details_and_exit(argv[0], e);
  }

  return 0;
}

