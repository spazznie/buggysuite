#include <libplayerc++/playerc++.h>
#include <libplayerc++/playerc++.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include "cmdline_parsing.h"
#include "common_functions.h"
#include "occupancy_grid.h"

using namespace PlayerCc;
using namespace std;
 
double PI = atan(1)*4;

// Thresholds for checking if two values are equal
double THRESHOLD = 0.05;       // Distance threshold
double THETA_THRESHOLD = 0.05;
double THETA_EPSILON = 0.01;

// conversion factor for scaling
double CONVERSION = 1; 
// the length of the robot
double ROBOT_LENGTH = 0.4; 
// Extra insurance to prevent smashing into walls
double SLACK = 0.2;
// max range of sensor
double RANGE_MAX = 8.0;
// how fast to move forward
double FORWARD_RATE = 0.2; 
// how fast to turn robot
double TURN_RATE = 0.3; 

enum state {
  MTG_SENSE,// Sensing phase of MTG to determine V(q)
  MTG_MOVE, // Moving to node phase of MTG
  BF_START, // Begin boundary following
  BF_SENSE, // Sensing phase of BF. Senses only one side based on direction
  BF_MOVE,  // Moving to node phase of BF
}; 

enum Direction {
  CLOCKWISE,
  COUNTERCLOCKWISE,
};

// tells the robot to go to the point
// return r_dot and theta_dot that can be used as inputs to setSpeed
int go_to_point(double goal_x, double goal_y, double robot_x, double robot_y,
    double robot_theta, double& r_dot, double& theta_dot){
    //find the distance from the current point to the goal point
  
    r_dot = 0;
    int theta_dot_sign = 1;
    //depending on the sign of the thetas, decide which direction to rotate
    double dx = goal_x - robot_x;
    double dy = goal_y - robot_y;
    double Dx = dx / sqrt((dx*dx) + (dy*dy));
    double Dy = dy / sqrt((dx*dx) + (dy*dy));
    double Hx = cos(robot_theta);
    double Hy = sin(robot_theta);
    double unit_dist  = (sqrt(pow((Dx - Hx),2) + pow((Dy - Hy),2)));
    double cross_z = Hx*Dy - Hy*Dx;
    theta_dot_sign = ((cross_z < 0)? -1 :1);
    theta_dot = 0.3;
    if (unit_dist < THETA_THRESHOLD){
      //the case where the theta is correct
      theta_dot = 0;
      //set r_dot to the maximum
      double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
      r_dot = std::min(distance_to_goal + 0.1, FORWARD_RATE);
    } else if (unit_dist < THETA_EPSILON) {
      //in a more acceptable range, so allow some movement
      theta_dot = 0;
      r_dot = FORWARD_RATE;
    } else if (unit_dist < 0.3) {
      //snap to minimum rotation requirement
      theta_dot = theta_dot_sign * 0.3;
      r_dot = 0;
    } else {
      //restrict to maximum rotation requirement
      theta_dot = theta_dot_sign * std::min(unit_dist, 0.7);
      r_dot = 0;
    }
    return 0;
}

// Given current destination and desired destination, determine if we are
// within t of the goal. 
int reached_within(double goal_x, double goal_y, double robot_x, double robot_y, double t) {
   
    double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
    return  ((distance_to_goal <= t) ? 1: 0);
}

// Given current destination and desired destination, determine if we are
// within THRESHOLD of the goal.
int reached(double goal_x, double goal_y, double robot_x, double robot_y) {
    // apply the distance between two points formula and determine if it is
    // <= THRESHOLD
    return reached_within(goal_x, goal_y, robot_x, robot_y, THRESHOLD);
}

// Calculate distance between two points.
double distance(double goal_x, double goal_y, double robot_x, double robot_y) {
  
    double distance_to_goal = (sqrt(pow((goal_x - robot_x),2) + pow((goal_y - robot_y),2)));
    return distance_to_goal;
}

// Convert theta to [0, 2*PI) range
double convertTheta(double theta) {
  return fmod(theta + 2 * PI, 2 * PI);
}

// Find smallest theta between two angles
double diffTheta(double theta_1, double theta_2) {
  double diff = fabs(theta_1 - theta_2);
  if (diff > PI) {
    diff = 2 * PI - diff;
  }
  return diff;
}

// Check if there is a obstacle in the way from the robot's current
// position to node
bool wallInFront(vector<double> range_data, vector<double> bearing_data,
                 double robot_x, double robot_y, double robot_theta,
                 double node_x, double node_y) {
  double theta;

  // Find theta towards node with respect to xy frame
  double node_theta = convertTheta(atan2(node_y - robot_y, node_x - robot_x));
  // Find distance towards node
  double node_dist = distance(node_x, node_y, robot_x, robot_y);
  // Look for bearing datapoint corresponding to a theta close to node_theta
  for (unsigned int i = 0; i < bearing_data.size(); i++) {
    theta = convertTheta(robot_theta + bearing_data[i]);
    if (diffTheta(node_theta, theta) < 
        std::max(std::min(4 * THETA_THRESHOLD / node_dist, 2 * THETA_THRESHOLD), THETA_THRESHOLD / 2)) {
      // Check if any obstacle between robot and node
      for (unsigned int j = i - 2; j < i + 2; j++) {
        if (range_data[j] < node_dist) {
          return true;
        }
      }
      return false;
    }
  }
  // If no data, play it safe and assume wall in front
  return true;
}

// Check if robot has circumnavigated obstacle by comparing robot's
// current position to when it started first circumnavigating
bool circumnavigated(double circum_x, double circum_y,
                     double robot_x, double robot_y) {
  return (distance(circum_x, circum_y, robot_x, robot_y) < ROBOT_LENGTH + SLACK);
}

// Find circumnavigation direction given robot's current position, qf,
// and coordinate of next node
Direction findDirection(double goal_x, double goal_y,
                        double robot_x, double robot_y,
                        double next_node_x, double next_node_y) {
  // Construct first vector
  double v1_x = goal_x - robot_x;
  double v1_y = goal_y - robot_y;
  // Construct second vector
  double v2_x = next_node_x - robot_x;
  double v2_y = next_node_y - robot_y;

  // Use sign of cross product (v1 x v2) to determine direction
  // Positive = CLOCKWISE, Negative = COUNTERCLOCKWISE
  return ((v1_x * v2_y - v1_y * v2_x < 0) ? COUNTERCLOCKWISE:CLOCKWISE);
}

// Helper function to get x-coordinate given starting point, theta, and range
double getXCoord(double robot_x, double robot_theta, double range) {
  return range * cos(robot_theta) + robot_x;

}

// Helper function to get y-coordinate given starting point, theta, and range
double getYCoord(double robot_y, double robot_theta, double range) {
  return range * sin(robot_theta) + robot_y;
}

// Find LTG nodes given range and bearing data
void calculateLTG(double prev_theta, double robot_theta,
                  double robot_x, double robot_y,
                  double qf_x, double qf_y, double &d_c,
                  vector<double> range_data, vector<double> bearing_data,
                  vector<double> &LTG_points_x, vector<double> &LTG_points_y,
                  bool &done_sensing) {
  // Coordinate of LTG node
  double node_x, node_y, buffer_x, buffer_y, theta;
  printf("Calculating LTG nodes\n");
  // Find theta towards goal point with respect to xy frame
  double goal_theta = convertTheta(atan2(qf_y - robot_y, qf_x - robot_x));
  // Find distance towards goal point
  double goal_dist = distance(qf_x, qf_y, robot_x, robot_y);
  // Current values
  double cur_range, cur_bearing, cur_slope;
  double prev_x = 999;
  double prev_y = 999;

  // Do not calculate if robot_theta is close to last calculated prev_theta
  if (diffTheta(robot_theta, prev_theta) > THETA_THRESHOLD || prev_theta == -1) {
    double prev_range = range_data[1];
    double prev_bearing = bearing_data[1];
    // bearing values skew slope values, so ignore for now
    double prev_slope = (prev_range - range_data[0]);
 
    // Scan through range_data, looking for discontinuities
    for (unsigned int i = 2; i < range_data.size(); i++) {
      cur_range = range_data[i];
      cur_bearing = bearing_data[i];
      cur_slope = (cur_range - prev_range);
      // Find theta
      theta = convertTheta(robot_theta + cur_bearing);
      // Find coordinates
      node_x = getXCoord(robot_x, theta, cur_range);
      node_y = getYCoord(robot_y, theta, cur_range);

      // Discontinuity will occur at sudden changes in slope
      // If node has already been seen in this data, skip
      if (fabs(cur_slope - prev_slope) > 2 * THETA_THRESHOLD
          && (fabs(prev_x - node_x) > 0.1) && (fabs(prev_y - node_y) > 0.1)) {
        // To avoid hitting obstacle, move slightly to left or right of contact point,
        // depending on whichever range_data is farther
        if (prev_range > cur_range) {
          // Move sligtly right
          buffer_x = (ROBOT_LENGTH + SLACK) * cos(theta - PI / 2);
          buffer_y = (ROBOT_LENGTH + SLACK) * sin(theta - PI / 2);
          node_x += buffer_x;
          node_y += buffer_y;
        } else {
          // Move slightly left
          // Use values from previous point
          theta = convertTheta(robot_theta + prev_bearing);
          buffer_x = (ROBOT_LENGTH + SLACK) * cos(theta + PI / 2);
          buffer_y = (ROBOT_LENGTH + SLACK) * sin(theta + PI / 2);
          node_x = getXCoord(robot_x, theta, prev_range) + buffer_x;
          node_y = getYCoord(robot_y, theta, prev_range) + buffer_y;
        }
        // Make sure there is no wall between robot and destination
        if (!wallInFront(range_data, bearing_data, robot_x, robot_y, robot_theta, node_x, node_y)) {
          // Add point to list of LGT nodes
          LTG_points_x.push_back(node_x);
          LTG_points_y.push_back(node_y);
          prev_x = node_x;
          prev_y = node_y;
        }
      } else if (diffTheta(theta, goal_theta) < THETA_THRESHOLD) {
        // If goal is past sensor range, add "reflected" goal position only if not
        // on obstacle point
        if (cur_range == RANGE_MAX && cur_range < goal_dist) {        
          // Add reflected goal position
          LTG_points_x.push_back(node_x);
          LTG_points_y.push_back(node_y);
          // No need to look further since we found best possible node
          done_sensing = true;
          break;
        } else if (goal_dist < cur_range) {
          // Add goal point directly to list of LTG nodes
          LTG_points_x.push_back(qf_x);
          LTG_points_y.push_back(qf_y);
          // No need to look further since we found best possible node
          done_sensing = true;
          break;
        } else {
          // If obstacle boundary is in between current position and goal,
          // update d_c
          d_c = distance(qf_x, qf_y, node_x, node_y);
        }
      } 
      prev_range = cur_range;
      prev_bearing = cur_bearing;
      prev_slope = cur_slope;
    } 
  } 
}

// Determine closest node in LTG to goal
// Return coordinates of node as well as distance
vector<double> findNodeClosest(double qf_x, double qf_y,
                               vector<double> &LTG_points_x, vector<double> &LTG_points_y) {
  double min_dist = 9999;
  double min_x = 0;
  double min_y = 0;
  double cur_x, cur_y, cur_dist;
  unsigned int size = LTG_points_x.size();
  for (unsigned int i = 0; i < size; i++) {
    cur_x = LTG_points_x[i];
    cur_y = LTG_points_y[i];
    cur_dist = distance(qf_x, qf_y, cur_x, cur_y);
    if (cur_dist < min_dist) {
      min_dist = cur_dist;
      min_x = cur_x;
      min_y = cur_y;
    }
  }
 
  // Construct coord and dist of closest node
  vector<double> coord;
  coord.push_back(min_x);
  coord.push_back(min_y);
  coord.push_back(min_dist);
  return coord;
}

int main(int argc, char **argv) {

  //wrote our own arg parser .-.
  if (argc != 7) {
    cout << "Usage: ./bugI -p <port> -x <goal_x> -y <goal_y> " << endl;
    exit(1);
  }
  gPort = atoi(argv[2]);
  double qf_x = atof(argv[4]); //q_f 
  double qf_y = atof(argv[6]); //q_f
  cout << "heading to goal " << qf_x << "," << qf_y  << endl;
  
  // Store path data
  string pathfilename = "../data/pathdata/tangentbug"; 
  ofstream pathfile;
  pathfile.open(pathfilename.c_str());
  cout << "writing path data to" << pathfilename << endl;
  int stepcount = 0;

  double lower_left[2] = {-8, -8};
  double upper_right[2] = {8, 8};
  double cell_size = 0.05;

  SimpleOccupancyGrid oc(lower_left, upper_right, cell_size);

  try {
    // Initialize connection to player
    PlayerClient robot(gHostname, gPort);
    Position2dProxy pp(&robot, gIndex);
    LaserProxy lp(&robot, gIndex); 

    int num_attempts = 20;
    if(!check_robot_connection(robot, pp, num_attempts)) 
      exit(-2);

    // The state our FSM is in
    int state = 0; 
    // Robot pose values
    double robot_x, robot_y, robot_theta; 
    // Robot speed setting values
    double r_dot, theta_dot; 

    // Count the number of step robot took
    int step = 0;
    
    // Values that can be updated in the loop
    // By default, assume goal is reachable. update this as seen fit.
    bool unreachable = false;
    
    // First find where robot is currently positioned
    robot.Read();
 
    // Variables for MTG
    // Closest robot has come to the goal
    double d_min = distance(qf_x, qf_y, pp.GetXPos(), pp.GetYPos());
    // Starting theta position for MTG_SENSE
    double start_theta = convertTheta(pp.GetYaw());
    // Previous theta position from last measured LTG
    double prev_theta = -1;
    // Difference between start_theta and robot_theta
    double diff_theta;
    // Coordinates of LTG nodes
    vector<double> LTG_points_x, LTG_points_y;
    // Add current position to LTG
    LTG_points_x.push_back(pp.GetXPos());
    LTG_points_y.push_back(pp.GetYPos());
    // Bool to determine if we can quit sensing early
    bool done_sensing = false;
    // Coordinate of next node to visit, as well as distance to goal
    vector<double> next_node;

    // Variables for BF
    // If there is a closer point to qf on an obstacle boundary, denote
    // by c, and let d_c = distance from c to qf
    double d_c;
    // Circumnavigation direction
    Direction direction;
    // Circumnavigation starting point from BF_START
    double circum_x, circum_y;
    // Bool to determine if we are in first pass of BF
    bool isFirstPass = true;
    // Number of steps taken from circum point
    double bf_step;
    while(1) {	
      // Read from the proxies; YOU MUST ALWAYS HAVE THIS LINE
      robot.Read();
			
      // Collect robot pose data
      double pose[3] = { pp.GetXPos(), pp.GetYPos(), pp.GetYaw() };

      robot_x = pose[0];
      robot_y = pose[1];
      robot_theta = convertTheta(pose[2]);

      // Write data to file every 5 steps
      stepcount = stepcount % 5;
      if (stepcount == 0) {
        pathfile << robot_x << " " << robot_y << endl;
        //cout << "writing path data " << endl;
      }
      stepcount++;

      // Collect laser data
      unsigned int n = lp.GetCount();
						
      if (n==0)
        // No data available
        continue;
			
       vector<double> range_data(n);
       vector<double> bearing_data(n);
       for(uint i=0; i<n; i++) {
         range_data[i] = lp.GetRange(i);
         bearing_data[i] = lp.GetBearing(i);
       }

      // Check for completeness
      if (reached(qf_x, qf_y, robot_x, robot_y)) {
        cout << "goal reached " << endl;
        exit(0);
      } else if (unreachable) { 
        cout << "goal can't be reached. Failure" << endl;
        exit(1);
      } else {
        switch (state)  {
          case MTG_SENSE:
            diff_theta = convertTheta(robot_theta - start_theta);
            // Stop turning when 2/3 of a circle has been made. We have enough data to
            // determine LTG of V(q)
            if (fabs(diff_theta - 4 * PI / 3) < THETA_THRESHOLD || done_sensing) {
              // Calculate LTG if needed
              if (!done_sensing) {
                calculateLTG(prev_theta, robot_theta, robot_x, robot_y, qf_x, qf_y, d_c,
                             range_data, bearing_data, LTG_points_x, LTG_points_y, done_sensing);
              }
              // Reset values
              done_sensing = false;
              prev_theta = -1;
              
              // Find closest node in LTG to goal
              next_node = findNodeClosest(qf_x, qf_y, LTG_points_x, LTG_points_y);
              
              // If node is closer than d_min, move to Node N
              if (next_node[2] < d_min - THRESHOLD) {
                d_min = next_node[2];
                state = MTG_MOVE;
                printf("Entering MTG_MOVE\n");
                printf("Going to %f %f\n", next_node[0], next_node[1]);
              } else {
                // Otherwise, we are stuck and must backup
                state = BF_START;
                printf("Entering BF_START\n");
              }
            } else if (fmod(diff_theta, 2 * PI / 3) < THETA_THRESHOLD) {
              // Calculate LTG at start or after 1/3 of a circle has been made
              calculateLTG(prev_theta, robot_theta, robot_x, robot_y, qf_x, qf_y, d_c,
                           range_data, bearing_data, LTG_points_x, LTG_points_y, done_sensing);
            }
            // Turn counterclockwise to gather data to build visibility set
            pp.SetSpeed(0,TURN_RATE);
            
            break;
          case MTG_MOVE: 
            // Move toward next_node coordinates until close enough
            if (reached(next_node[0], next_node[1], robot_x, robot_y)) {
              // Go back to MTG_SENSE state to find new node
              state = MTG_SENSE;
              printf("Entering MTG_SENSE\n");
              start_theta = robot_theta;
              // Clear values
              LTG_points_x.clear();
              LTG_points_y.clear();
              
              // Add current position to LTG
              LTG_points_x.push_back(robot_x);
              LTG_points_y.push_back(robot_y);
            } else {
              step++;
              // Just keep moving toward the next_node until we encounter a wall
              go_to_point(next_node[0], next_node[1], robot_x, robot_y,
                          robot_theta, r_dot, theta_dot);
              pp.SetSpeed(r_dot, theta_dot);
            }
            break;
          case BF_START: 
            // If there is closer point to qf on obstacle boundary, denoted c,
            // update d_min = d_c
            d_min = d_c;
            // Search LTG for node closest to goal, excluding current point
            LTG_points_x.erase(LTG_points_x.begin());
            LTG_points_y.erase(LTG_points_y.begin());
            next_node = findNodeClosest(qf_x, qf_y, LTG_points_x, LTG_points_y);
          
            // Remember next node so we know whether we circumnavigated obstacle
            circum_x = next_node[0];
            circum_y = next_node[1];
            printf("start circum point at %f %f\n", circum_x, circum_y);
            // Determine circumnavigation direction
            direction = findDirection(qf_x, qf_y, robot_x, robot_y,
                                      next_node[0], next_node[1]);
            isFirstPass = true;
            state = BF_MOVE;
            printf("Entering BF_MOVE\n");
            break;
          case BF_SENSE:
            // Look for next node closest to goal
            if (isFirstPass) {
              bf_step = step;
              isFirstPass = false;
            }
            
            // Since we want to maintain circumnavigation direction, only need to turn 45 degrees     
            diff_theta = diffTheta(robot_theta, start_theta);
            // Stop turning when 1/8 of a circle has been made in either direction.
            if (fabs(diff_theta - PI / 4) < 2 * THETA_THRESHOLD || done_sensing) {
              // Calculate LTG if needed
              if (!done_sensing) {
                calculateLTG(prev_theta, robot_theta, robot_x, robot_y, qf_x, qf_y, d_c,
                             range_data, bearing_data, LTG_points_x, LTG_points_y, done_sensing);
              }          
              // Reset values
              done_sensing = false;
              prev_theta = -1;
  
              // Find closest node in LTG that maintains circumnavigation direction
              next_node = findNodeClosest(qf_x, qf_y, LTG_points_x, LTG_points_y);
              // If no nodes are found, then unreachable
              if (next_node[2] == 9999) {
                unreachable = true;
              } else if (next_node[2] < d_min) {
                // If node is closer than d_min, return to MTG_MOVE
                d_min = next_node[2];
                state = MTG_MOVE;
                printf("Entering MTG_MOVE\n");
                printf("Going to %f %f\n", next_node[0], next_node[1]);
              } else {
                // Otherwise, continue to BF_MOVE and move to next node
                state = BF_MOVE;
                printf("Entering BF_MOVE\n");
                printf("Going to %f %f\n", next_node[0], next_node[1]);
              }
            } 
            switch (direction) {
              case CLOCKWISE:
                pp.SetSpeed(0, -TURN_RATE);
                break;
              case COUNTERCLOCKWISE:       
                pp.SetSpeed(0,TURN_RATE);
                break;
            }
            break;
          case BF_MOVE:
            // If obstacle circumnavigated, then unreachable
            if (circumnavigated(circum_x, circum_y, robot_x, robot_y)
                && !isFirstPass && (step - bf_step > 50)) {
              printf("Obstacle circumnavigated\n");
              unreachable = true;
            // Move toward next_node coordinates until close enough
            } else if (reached(next_node[0], next_node[1], robot_x, robot_y)) {
              // Go back to BF_SENSE state to find next node
              state = BF_SENSE;
              printf("Entering BF_SENSE\n");
              start_theta = robot_theta;
              // Clear values
              LTG_points_x.clear();
              LTG_points_y.clear();
              // Sense initial values
              calculateLTG(prev_theta, robot_theta, robot_x, robot_y, qf_x, qf_y, d_c,
                           range_data, bearing_data, LTG_points_x, LTG_points_y, done_sensing);
            } else {
              step++;
              // Just keep moving toward the next_node until we encounter a wall
              go_to_point(next_node[0], next_node[1], robot_x, robot_y,
                          robot_theta, r_dot, theta_dot);
              pp.SetSpeed(r_dot, theta_dot);
            }
            break;            
        }
     }
   }
  } catch(PlayerError e) {
    write_error_details_and_exit(argv[0], e);
  }

  return 0;
}
