'''
plotpaths.py
the path plotter portion of the Buggy Suite
Takes a list of polygon vertices where each polygon is delimited by a
    newline #Polygon <n>, where n is the polygon number
file must end with #End
Polygon vertices must be given in either clockwise or counterclockwise traversal
plots all polygons on an image named configs/planning.png in the BuggySuite 
    folder
polygon vertex x and y values must be in the range [0,6.5]
if planning.png already exists, it is renamed 

usage: plotpolygons [data/polygondata/]<polygon data file>
'''
import os
import pylab
import matplotlib.pyplot as plt
import sys
import matplotlib.image as mpimg

def plotpolygons(filename):

    img = plt.imread('base_image.png')
    imgplot = plt.imshow(img, origin='lower')
    filepath = '../data/polygondata/' + filename
    f = open(filepath, 'r')
    
    xarray = []
    yarray = []
    firstx = 0 #the first coordinate of the current polygon
    firsty = 0 #the first coordinate of the current polygon
    newPoly = True
    for line in f:
        if line[0] == '#':
            #new polygon started, close the old one
            xarray.append(firstx)
            yarray.append(firsty)
            #plot the polygon
            plt.plot(xarray, yarray, 'k', linewidth=2.5)
            xarray = []
            yarray = []
            newPoly = True
        else:
            a,b = line.split()
            a = (float(a) + 2.1)*40 #scaling
            b = (float(b) + 2.1)*40 #scaling
            if newPoly:
                firstx = a
                firsty = b
                newPoly = False
            xarray.append(a)
            yarray.append(b)

    plt.axis('off')
    plt.savefig('../configs/planning.png', transparent=True, bbox_inches='tight', \
        pad_inches=0)
    
    
if __name__=="__main__":
    if len(sys.argv) == 1:
        print 'usage:plotpolygons <polygon data file>'
        exit(0)
    filename = sys.argv[1]
    if os.path.isfile('../configs/planning.png'):
        os.remove('../configs/planning.png')
    plotpolygons(filename)